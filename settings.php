<!DOCTYPE html>
<html>
    <head>
        <title>TA-NA</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/style.css" rel="stylesheet" type="text/css"/>

        <script src="js/jquery-1.12.4.js" type="text/javascript"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        

    </head>
    <body>
             
    <div id="wrap">  
       <?php require_once ('_block/_header.php');?>
        <div class="container" id="settings">
            <div class="tabs-office">
            <ul class="tabs">
                <li><a href="myOffice-myAdvert.php" title="myAdvert" class="tab ">Мои обьявления</a></li> 
                <li><a href="myMessage.php" title="myMessage" class="tab">Мои сообщения</a></li> 
                <li><a href="settings.php" title="settings" class="tab active">Настройки</a></li> 
            </ul>
        </div> 
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="main-settinds">Имя/Организация</label> 
                        <div class="input-group">
                        <span class="input-group-addon"><span class="glyphicon glyphicon-pencil"></span></span>
                        <input type="text" class="form-control"/>
                        </div>
                        <label class="main-settinds">Город</label>
                       <div class="input-group">
                        <span class="input-group-addon"><span class="glyphicon glyphicon-pencil"></span></span>
                        <input type="text" class="form-control"/>
                        </div> 
                      <label class="main-settinds">Контакты</label>  
                      <div class="input-group">
                        <span class="input-group-addon"><span class="glyphicon glyphicon-pencil"></span></span>
                        <input type="text" class="form-control"/>
                        </div>
                      <label class="main-settinds">E-mail</label>
                      <div class="input-group">
                        <span class="input-group-addon"><span class="glyphicon glyphicon-pencil"></span></span>
                        <input type="text" class="form-control"/>
                        </div>
                      <p><input type="checkbox" name="a" value="По всему району" chacked>Получать новости на почту</p>
                      <p><input type="checkbox" name="a" value="По всей области" chacked>Получать сообщения на почту</p>
                      <label class="main-settinds">Старый пароль</label> 
                      <div class="input-group">
                       <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
                        <input type="password" class="form-control"/>
                      </div>
                      <label class="main-settinds">Новый пароль</label> 
                      <div class="input-group">
                       <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
                        <input type="password" class="form-control"/>
                      </div>
                      <button type="button" class="btn btn-default navbar-btn">Сменить пароль</button>
                    </div>
                </div>
                <div class="col-md-6">
                    <img src="image/register.jpg" alt=""/>
                       
                </div>
                
            </div>
            <div class="row">
                <div class="col-md-4">
                 <button type="button" class="btn btn-default navbar-btn">Удалить учётную запись</button>   
                </div>
                <div class="col-md-8">
                   <button type="button" class="btn btn-default navbar-btn">Сохранить изменения</button>  
                </div>
            </div>
            
        </div>
        <?php require_once ('_block/_footer.php');?>
    </div>    
    </body>
    
</html>

