<header>
    <nav class="navbar">
        <div id="logo">
            <a href="index.php.html">
                <img src="\image\малюнок.png" class="img-circle" alt="">
            </a>
        </div>
        <div id="menu">
            <ul>
                <li> <a <?php if ($activeMenu=='project') echo 'class="active"';?> href="/project.php">О ПРОЕКТЕ</a></li>
                <li><a <?php if ($activeMenu=='helpideas') echo 'class="active"';?> href="/helpideas.php">ПОМОЩЬ ИДЕЕ</a></li>
                <li><a <?php if ($activeMenu=='partners') echo 'class="active"';?> href="/partners.php">НАШИ ПАРТНЕРЫ</a></li>
                <li><a <?php if ($activeMenu=='news') echo 'class="active"';?> href="/news.php">НОВОСТИ</a></li>
                <li><a <?php if ($activeMenu=='wanted') echo 'class="active"';?> href="/wanted.php" >РОЗЫСК</a></li>
                <li><a <?php if ($activeMenu=='advert') echo 'class="active"';?> href="/advert.php">ОБЬЯВЛЕНИЯ</a></li>
            </ul>
        </div>
        <div id="right-head-block">
            <div id="btn-profile">
                <img src="\image\рисунок.png" class="img-circle" alt="">
                <a href="myOffice-myAdvert.php">МОЙ КАБИНЕТ</a>
            </div>
            <div id="btn-search">
                <span class="glyphicon glyphicon-search">
                </span>
            </div>
            <div id="btn-help">
                <a href="#">ХОЧУ ПОМОЧЬ</a>
            </div>
        </div>
    </nav>
</header>

