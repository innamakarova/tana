<footer>
    <div class="row">
        <div class="col-md-6">
            <div class="menu-footer"
    <ul>
        <li><a <?php if ($activeMenu=='project') echo 'class="active"';?> href="/project.php">О ПРОЕКТЕ</a></li>
        <li><a <?php if ($activeMenu=='helpideas') echo 'class="active"';?>href="/helpideas.php">ПОМОЩЬ ИДЕЕ</a></li>
        <li><a <?php if ($activeMenu=='partners') echo 'class="active"';?>href="/partners.php">НАШИ ПАРТНЕРЫ</a></li>
        <li><a <?php if ($activeMenu=='news') echo 'class="active"';?>href="/news.php">НОВОСТИ</a></li>
        <li><a <?php if ($activeMenu=='wanted') echo 'class="active"';?>href="/wanted.php">РОЗЫСК</a></li>
        <li><a <?php if ($activeMenu=='myOffice-myAdvert') echo 'class="active"';?>href="/myOffice-myAdvert.php">МОЙ КАБИНЕТ</a></li>
      </ul>
        </div>
        </div> 
            <div class="col-md-6">  
                <div class="icon-footer">
    <div id="icon-tel">
        <span class="glyphicon glyphicon-earphone"></span>
      +38 (099) 57 57 577 
    </div>
    <div id="icon-mail">
        <span class="glyphicon glyphicon-envelope">
         </span>
        <a href="mailto:tana@email">tana@email</a>
    </div>
       </div>         
      </div>      
    </div>
   
</footer>   
