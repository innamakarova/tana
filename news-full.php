<!DOCTYPE html>
<html>
    <head>

        <title>TA-NA</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/style.css" rel="stylesheet" type="text/css"/>
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>

        <script src="js/jquery-1.12.4.js" type="text/javascript"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script>

    </head>
    <body>
        <div id="wrap">
            <?php require_once ('_block/_header.php'); ?>
            <div class="container" id="newsFull">
                <?php
                $newsfull = ['img' => 'image/image1.jpg', 'title' => 'Заголовок', 'dateadd' => '10.10.2020', 'body' => 'Текст новини']
                ?>
                <div class="row">
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-2">
                                <img src="<?= $newsfull['img']; ?>" alt="">

                            </div>
                            <div class="col-md-10">
                                <div class="row">
                                    <div class="col-md-8">
                                        <h3><?= $newsfull['title']; ?></h3>
                                    </div>
                                    <div class="col-md-4">
                                        <p><?= $newsfull['dateadd']; ?></p>
                                    </div>

                                    <div class="col-md-12">
                                        <p> <?= $newsfull['body']; ?></p>
                                    </div>

                                </div>

                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-12 social">
                                <h3>Поделиться в сетях</h3> 
                                <i class="fa fa-facebook-square" ></i>
                                <i class="fa fa-twitter-square"></i>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="row">
                                <div class="col-md-12">
                                    <h3>Останні новини</h3>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <img src="<?= $newsfull['img']; ?>" alt="">  
                                </div>
                                <div class="col-md-8">
                                    <h4><?= $newsfull['title']; ?></h4>
                                    <p> <?= $newsfull['body']; ?></p>
                                    <p><a href="#" class="moredetails">Подробнее </a></p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <img src="<?= $newsfull['img']; ?>" alt="">  
                                </div>
                                <div class="col-md-8">
                                    <h4><?= $newsfull['title']; ?></h4>
                                    <p> <?= $newsfull['body']; ?></p>
                                    <p><a href="#" class="moredetails">Подробнее </a></p>
                                </div>
                            </div> 
                            <div class="row">
                                <div class="col-md-4">
                                    <img src="<?= $newsfull['img']; ?>" alt="">  
                                </div>
                                <div class="col-md-8">
                                    <h4><?= $newsfull['title']; ?></h4>
                                    <p> <?= $newsfull['body']; ?></p>
                                    <p><a href="#" class="moredetails">Подробнее </a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>



<?php require_once ('_block/_footer.php'); ?>
        </div>
    </body>

</html>



