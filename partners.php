<!DOCTYPE html>
<html>
    <head>
        <title>TA-NA</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/style.css" rel="stylesheet" type="text/css"/>

        <script src="js/jquery-1.12.4.js" type="text/javascript"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script>


    </head>
    <body>
        <?php
       $activeMenu='partners';
               ?>
        <div id="wrap" > 
            <?php require_once ('_block/_header.php'); ?>
            <div class="container" id="mainPartners">
                <?php
                $mainPartners = [
                    ['img' => 'image/partners/partner1.jpg', 'href' => 'www.google.com/search?q=1'],
                    ['img' => 'image/partners/partner2.jpg', 'href' => 'www.google.com/search?q=2'],
                    ['img' => 'image/partners/partner3.jpg', 'href' => 'www.google.com/search?q=3'],
                    ['img' => 'image/partners/partner4.jpg', 'href' => 'www.google.com/search?q=4'],
                    ['img' => 'image/partners/partner5.jpg', 'href' => 'www.google.com/search?q=5'],
                    ['img' => 'image/partners/partner6.jpg', 'href' => 'www.google.com/search?q=6'],
                    ['img' => 'image/partners/partner7.jpg', 'href' => 'www.google.com/search?q=7'],
                    ['img' => 'image/partners/partner8.jpg', 'href' => 'www.google.com/search?q=8'],
                    ['img' => 'image/partners/partner9.jpg', 'href' => 'www.google.com/search?q=9'],
                    ['img' => 'image/partners/partner10.jpg', 'href' => 'www.google.com/search?q=10'],
                    ['img' => 'image/partners/partner11.jpg', 'href' => 'www.google.com/search?q=11'],
                    ['img' => 'image/partners/partner12.jpg', 'href' => 'www.google.com/search?q=12'],
                    ['img' => 'image/partners/partner13.jpg', 'href' => 'www.google.com/search?q=13'],
                    ['img' => 'image/partners/partner14.jpg', 'href' => 'www.google.com/search?q=14'],
                    ['img' => 'image/partners/partner15.jpg', 'href' => 'www.google.com/search?q=15'],
                ];
                ?>
                <div class="row">
                    <div class="col-md-12">
                        <?php foreach ($mainPartners as $partner) : ?>
                            <a href="<?=$partner['href']?>">
                                <img src="<?= $partner['img']?>" class="mainPartners" alt="">
                            </a>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div> 
            <?php require_once ('_block/_footer.php'); ?>
        </div>
    </body>
</html>