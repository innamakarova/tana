<!DOCTYPE html>
<html>
    <head>
        <title>TA-NA</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/style.css" rel="stylesheet" type="text/css"/>
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <script src="js/jquery-1.12.4.js" type="text/javascript"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script>


    </head>
    <body>
        <?php
       $activeMenu='news';
               ?>
        <div id="wrap">
            <?php require_once ('_block/_header.php'); ?>

            <div class="container" id="news">
                <?php
                $news = [
                    ['img' => 'image/image1.jpg', 'title' => 'Заголовок1', 'dateadd' => '10.10.2020', 'body' => 'Текст новини1'],
                    ['img' => 'image/image1.jpg', 'title' => 'Заголовок2', 'dateadd' => '05.10.2020', 'body' => 'Текст новини2'],
                    ['img' => 'image/image1.jpg', 'title' => 'Заголовок3', 'dateadd' => '01.10.2020', 'body' => 'Текст новини3'],
                        ]
                ?>
                
                <?php foreach ($news as $newsItem) : ?>
                    <div class="row">
                        <div class="col-md-2">
                            <img src="<?= $newsItem['img']; ?>" alt="">
                        </div>
                        <div class="col-md-10">
                            <div class="row">
                                <div class="col-md-8">
                                    <h4><?= $newsItem['title']; ?></h4>
                                </div>

                                <div class="col-md-4">
                                    <p><?= $newsItem['dateadd']; ?></p>
                                </div>
                                <div class="col-md-12">
                                    <p> <?= $newsItem['body']; ?></p>
                                </div>
                                <a href="news-full.php" class="moredetails">Подробнее </a>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>

            <?php require_once ('_block/_footer.php'); ?>
        </div>
    </body>

</html>

