<!DOCTYPE html>
<html>
    <head>
        <title>TA-NA</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/all.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/style.css" rel="stylesheet" type="text/css"/>

        <script src="js/jquery-1.12.4.js" type="text/javascript"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
    </head>
    <body>
        <div id="wrap">  
            <form class="form-horizontal" role="form">
                <?php require_once ('_block/_header.php'); ?>
                <div class="container" id="advertAdd">
                    <?php
                    $advertAdd = [
                        'title' => 'Заголовок обьявления',
                        'name' => 'Имя/Организация',
                        'address' => 'Киев/Область',
                        'phone' => 'Номер телефона',
                        'description' => 'Описание',
                        'mainImage' => 'image/image1.jpg',
                        'images' => ['image/image1.jpg', 'image/image2.jpg', 'image/image3.jpg', 'image/image4.jpg'],
                    ];
                    ?>
                    <div class="row">
                        <div class="col-md-4">
                            <input type="file" name="file upload" id="file-input">
                        </div> 
                    </div>     
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-2">
                                    <?php foreach ($advertAdd['images'] as $img) : ?>
                                        <a href="#"><img src="<?= $img; ?>" class="advertImgs" alt=""></a>
                                    <?php endforeach; ?>
                                </div>
                                <div class="col-md-10">
                                    <a href="#"><img src="<?= $advertAdd['mainImage']; ?>" alt=""></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="form-group has-success has-feedback">
                                    <div class="col-md-12">
                                        <input type="text" class="form-control" placeholder="<?= $advertAdd['title']; ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="category">Категории</label>
                                    <div class="col-md-9">
                                        <select id="category" class="form-control" name="category" aria-invalid="false">
                                            <option value="0">Продукты питания</option>
                                            <option value="1">Медикаменты</option>
                                            <option value="2">Вещи</option>
                                            <option value="3">Предметы инвалидов</option>
                                        </select>
                                    </div>    
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="name">
                                        Имя/Организация</label>
                                    <div class="col-md-9">
                                        <input id="name" class="form-control" name="category" aria-invalid="false">

                                    </div>    
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="category">Местоположения</label>
                                    <div class="col-md-9">
                                        <select id="category" class="form-control" name="category" aria-invalid="false">
                                            <option value="0">Киев</option>
                                            <option value="1">Винница</option>
                                            <option value="2">Одесса</option>
                                            <option value="3">Харьков</option>
                                        </select>
                                    </div>    
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="name">
                                        Контакти</label>
                                    <div class="col-md-9">
                                        <input id="name" class="form-control" name="category" aria-invalid="false">
                                    </div>    
                                </div>


                            </div>
                        </div>
                    </div>

                    <h2>Описание</h2>      
                        <div class="row">
                            <div class="col-md-9">
                                <div class="form-group">
                                    
                                    <textarea  rows="8" class="form-control" id="contact-avtor"></textarea>
                                </div>    
                            </div>
                            <div class="col-md-3">
                                <div class="row">
                                    <div class="col-md-6">
                                        <input name="nyzhno" type="radio" value="nyzhno"> Нужно
                                    </div>
                                    <div class="col-md-6">
                                        <input name="nyzhno" type="radio" value="otdam"> Отдам
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <input type="button" class="btn btn-primary btn-circle" value="Опубликовать" id="circle">      
                                    </div>

                                </div>


                            </div>

                            </form>
                        </div>
                        <?php require_once ('_block/_footer.php'); ?>
                </div>  
        </div>  
    </div>    
</body>

</html>
