<!DOCTYPE html>
<html>
    <head>
        <title>TA-NA</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/all.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/style.css" rel="stylesheet" type="text/css"/>


        <script src="js/jquery-1.12.4.js" type="text/javascript"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script>


    </head>
    <body>


        <div id="wrap">  
            <?php require_once ('_block/_header.php'); ?>
            <div class="container" id="advertFull">
                <?php
                $advertFull = [
                    'title' => 'Заголовок обьявления',
                    'name' => 'Имя/Организация',
                    'address' => 'Киев/Область',
                    'phone' => 'Номер телефона',
                    'description' => 'Описание',
                    'mainImage' => 'image/image1.jpg',
                    'images' => ['image/image1.jpg', 'image/image2.jpg', 'image/image3.jpg', 'image/image4.jpg'],
                ];
                ?>
                <div class="row">
                    <div class="col-md-4">
                        <a href="#" class="othersadv">Другие обьявления автора</a>
                    </div>
                    <div class="col-md-8 socialNetworks">
                        <span>Поделиться в сетях</span>

                        <i class="fab fa-facebook-square" ></i>
                        <i class="fab fa-twitter-square"></i>
                        <i class="fab fa-viber"></i>
                        <i class="fab fa-instagram-square"></i>
                    </div>
                </div>    
                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-2">
                                <?php foreach ($advertFull['images'] as $img) : ?>

                                    <a href="#"><img src="<?= $img; ?>" class="advertImgs" alt=""></a>

                                <?php endforeach; ?>
                            </div>
                            <div class="col-md-10">
                                <a href="#"><img src="<?= $advertFull['mainImage']; ?>" alt=""></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group has-success has-feedback">
                            <input type="text" class="form-control" placeholder="<?= $advertFull['title']; ?>">

                        </div>
                        <div class="form-group has-success has-feedback">
                            <input type="text" class="form-control" placeholder="<?= $advertFull['name']; ?>">

                        </div>
                        <div class="form-group has-success has-feedback">
                            <input type="text" class="form-control" placeholder="<?= $advertFull['address']; ?>">

                        </div>
                        <div class="form-group has-success has-feedback">
                            <input type="text" class="form-control" placeholder="<?= $advertFull['phone']; ?>">

                        </div>


                    </div>
                </div>   
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group has-success has-feedback">
                            <h2>Описание</h2>

                        </div>  
                    </div>  
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <p>Сотрудники Главного управления контрразведывательной защиты интересов государства в сфере экономической безопасности СБ Украины блокировали деятельность группы лиц, причастных к подпольному изготовлению и реализации в различных регионах страны фальсифицированных медицинских препаратов, информирует enovosty.com/news.
                            По данным предварительного следствия, противоправную схему наладили несколько жителей Харькова, а также один из частных столичных дистрибьюторов лекарственных средств. Установлено, что с 2016 года дельцы организовали кустарное производство препаратов, применяемых для лечения пациентов с заболеваниями эндокринной системы, а также обезболивающих средств.</p>
                        <p>Цех, где происходило изготовление подделки из сырья сомнительного происхождения, они обустроили в арендованном помещении в промышленной зоне Харькова. Сбывали фальсификат под видом продукции известных отечественных производителей фармпрепаратов через интернет-ресурсы и аптечные сети.</p>
                        По предварительным подсчетам ежегодно дельцы реализовывали подделки в среднем на 15 000 000 гривен. Трех фигурантов дела, в том числе и представителя фирмы-дистрибьютора правоохранители задержали в Киеве и Харькове.
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="contact-avtor">Связаться с автором</label>
                            <textarea  rows="5" class="form-control" id="contact-avtor"></textarea>
                        </div>   
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">

                            <input type="checkbox" name="a" value="Прикрепить файл" id="attach-file" chacked>
                            <label for="attach-file">Прикрепить файл</label>
                        </div>
                    </div>   
                    <div class="col-md-6">
                        <button class="btn btn-success pull-right" >
                            Написать
                        </button> 
                    </div>    


                </div>
                <?php require_once ('_block/_footer.php'); ?>
                </body>
            </div>   
</html>
