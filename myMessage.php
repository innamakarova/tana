<!DOCTYPE html>
<html>
    <head>
        <title>TA-NA</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/style.css" rel="stylesheet" type="text/css"/>

        <script src="js/jquery-1.12.4.js" type="text/javascript"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script>


    </head>
    <body>

        <div id="wrap">  
            <?php require_once ('_block/_header.php'); ?>
            <div class="container" id="myMess">
                <div class="tabs-office">
            <ul class="tabs">
                <li><a href="myOffice-myAdvert.php" title="myAdvert" class="tab">Мои обьявления</a></li> 
                <li><a href="myMessage.php" title="myMessage" class="tab active">Мои сообщения</a></li> 
                <li><a href="settings.php" title="settings" class="tab">Настройки</a></li> 
            </ul>
        </div> 
            <div class="tabs-office-message">
                <a href="myMessage.php" title="myMessage" class="tab">Мои сообщения</a>
                <div class="row">
                    <div class="col-md-3">
                        <table class="table table-bordered">
                            <tr>
                                <th><a href="create-message.php">Новое сообщение</a></th>
                            </tr>
                            <tr>
                                <td><a href="#"> Полученные </a> </td>                        
                            </tr>
                            <tr>
                                <td><a href="#">Отправленные</a></td>
                            </tr>
                            <tr>
                                <td><a href="#">Архивные</a></td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-md-9">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Дата</th>
                                    <th>Кому/От кого</th>
                                    <th>Тема</th>
                                    <th>Статус</th>
                                    <th>Статус</th>
                                    <th>Действия</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Text</td>
                                    <td>Text</td>
                                    <td>Text</td>
                                    <td>Text</td>
                                    <td>Text</td>
                                    <td>
                                        <span class="glyphicon glyphicon-pencil"></span> 
                                        <span class="glyphicon glyphicon-trash"></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Text</td>
                                    <td>Text</td>
                                    <td>Text</td>
                                    <td>Text</td>
                                    <td>Text</td>
                                    <td>
                                        <span class="glyphicon glyphicon-pencil"></span> 
                                        <span class="glyphicon glyphicon-trash"></span>    

                                    </td>
                                </tr>   
                            </tbody>

                        </table>
                    </div>
                </div>
            </div>
            </div>
        
        <?php require_once ('_block/_footer.php'); ?>
        </div>
    </body>

</html>

