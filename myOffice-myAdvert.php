<!DOCTYPE html>
<html>
    <head>
        <title>TA-NA</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/style.css" rel="stylesheet" type="text/css"/>

        <script src="js/jquery-1.12.4.js" type="text/javascript"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        

    </head>
    <body>
      <?php
       $activeMenu='myOffice-myAdvert';
               ?>       
    <div id="wrap">  
       <?php require_once ('_block/_header.php');?>
        <div class="container" id="myOffice">
        <div class="tabs-office">
            <ul class="tabs">
                <li><a href="myOffice-myAdvert.php" title="myAdvert" class="tab active">Мои обьявления</a></li> 
                <li><a href="myMessage.php" title="myMessage" class="tab">Мои сообщения</a></li> 
                <li><a href="settings.php" title="settings" class="tab">Настройки</a></li> 
            </ul>
        </div> 
        <div class="row">
            <div class="col-md-3">
                <table class="table table-bordered">
                    <tr>
                        <th><a href="advert-add.php" class="table-advert">Подать обьявления</a></th>
                    </tr>
                    <tr>
                        <td><a href="#"> Активные </a> </td>                        
                    </tr>
                    <tr>
                        <td><a href="#">Архивные</a></td>
                    </tr>
                    <tr>
                        <td><a href="#">Избранное</a></td>
                    </tr>
                    
                </table>
            </div>
            <div class="col-md-9">
                <table class="table table-bordered">
                    <tr>
                        <th>Дата</th>
                        <th>Изображение</th>
                        <th>Название</th>
                        <th>Статус</th>
                        <th>Тип</th>
                        <th>Действия</th>
                    </tr>
                    <tr>
                        <td>Text</td>
                        <td>Text</td>
                        <td>Text</td>
                        <td>Активно</td>
                        <td>Нужно</td>
                        <td>
                            <span class="glyphicon glyphicon-pencil"></span> 
                            <span class="glyphicon glyphicon-trash"></span>
                        </td>
                    </tr>
                    <tr>
                        <td>Text</td>
                        <td>Text</td>
                        <td>Text</td>
                        <td>В архиве</td>
                        <td>Отдам</td>
                        <td>
                        <span class="glyphicon glyphicon-pencil"></span> 
                            <span class="glyphicon glyphicon-trash"></span>    
                           
                        </td>
                    </tr>
                </table>
            </div>
        </div> 
        </div>
        <?php require_once ('_block/_footer.php');?>
    </div>    
    </body>
    
</html>

