<!DOCTYPE html>
<html>
    <head>

        <title>TA-NA</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/style.css" rel="stylesheet" type="text/css"/>

        <script src="js/jquery-1.12.4.js" type="text/javascript"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script>

    </head>
    <body>
        <?php
       $activeMenu='advert';
               ?>
        <div id="wrap">
            <?php require_once ('_block/_header.php'); ?>
            <div class="container" id="advert">    
                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group has-success has-feedback">
                                    <input type="text" class="form-control" placeholder="Поиск по обьявлениям">
                                    <span class="glyphicon glyphicon-search form-control-feedback"></span>
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="search-city">Населённый пункт</label>
                                    <select id="search-city" class="form-control" name="search" aria-invalid="false">
                                        <option value="0">Все</option>
                                        <option value="1">Літин</option>
                                        <option value="2">Вінниця</option>
                                        <option value="3">Хмільник</option>

                                    </select>
                                </div>        

                            </div>
                            <div class="col-md-4">
                                <div class="form-group has-success has-feedback">
                                    <input type="submit" class="form-control" placeholder="ПОШУК" class = "btn btn-success">  
                                </div>   
                                <div class="form-group">
                                    <p><input type="checkbox" name="a" value="По всему району" chacked>По всему району</p>
                                    <p><input type="checkbox" name="a" value="По всей области" chacked>По всей области</p>
                                </div>  
                            </div>         
                        </div>
                        <div class="row">
                            <?php
//                            $categoryList = ['Продукты питания', 'Медицинская помощь', 'Предметы инвалидов', 'Работа',
//                                'Косметика и бытовая химия', 'Электроника', 'Предметы для дома', 'Животные', 'Хобби и отдых',
//                                'Волонтерская помощь', 'Детский мир', 'Одежда и обувь', 'Бесплатное образование', 'Скидки и акции'];
                            $categoryList =[
                                ['img'=>'image/image1.jpg','title'=>'Продукты питания'],
                                ['img'=>'image/image2.png','title'=>'Медицинская помощь'],
                                ['img'=>'image/image1.jpg','title'=>'Предметы инвалидов'],
                                ['img'=>'image/image1.jpg','title'=>'Работа'],
                                ['img'=>'image/image1.jpg','title'=>'Косметика и бытовая химия'],
                                ['img'=>'image/image1.jpg','title'=>'Электроника'],
                                ['img'=>'image/image1.jpg','title'=>'Предметы для дома'],
                                ['img'=>'image/image1.jpg','title'=>'Животные'],
                                ['img'=>'image/image1.jpg','title'=>'Хобби и отдых'],
                                ['img'=>'image/image1.jpg','title'=>'Волонтерская помощь'],
                                ['img'=>'image/image1.jpg','title'=>'Детский мир'],
                                ['img'=>'image/image1.jpg','title'=>'Одежда и обувь'],
                                ['img'=>'image/image1.jpg','title'=>'Бесплатное образование'],
                                ['img'=>'image/image1.jpg','title'=>'Скидки и акции'],
                                 ];
                            
                            ?>
                            <?php if (2>3) {
                                
                            } else {
                                
                            }?>
                            <?php foreach ($categoryList as $category) : ?>
                                <div class="col-md-6">
                                    <img src="<?=$category['img']?>" class="item-advert" alt="">
                                    <a href="#">
                                        <?=$category['title'] ?>
                                    </a>
                                </div>
                            <?php endforeach; ?>
                            
                        </div>     
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <?php
                            for ($i = 0; $i < 12; $i++) :
                                ?>
                                <div class="col-md-4">
                                    <img src="image/image1.jpg" class="will-give" alt="">
                                    <p>Текст обьявления</p>
                                </div>
                            <?php endfor; ?>

                        </div>
                    </div>   
                </div>
            </div>
            <?php require_once ('_block/_footer.php'); ?>
        </div>
    </body>
</html>
