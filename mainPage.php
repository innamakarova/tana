<!DOCTYPE html>
<html>
    <head>
        <title>TA-NA</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/style.css" rel="stylesheet" type="text/css"/>

        <script src="js/jquery-1.12.4.js" type="text/javascript"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        

    </head>
    <body>
        
        
       <div id="wrap"> 
       <?php require_once ('_block/_header.php');?>
           <div class="container" id="mainPage">
               <div class="row">
                   <div class="col-md-7">
                       <h2>TA NA - платформа для помощи друг другу</h2>
                       <p>ЗАРЕГИСТРИРУЙТЕСЬ, ЧТОБЫ СТАТЬ АЛЬТРУИСТОМ НА ПЛАТФОРМЕ TA NA</p>
                       <button type="button" class="btn btn-default">ЗАРЕГИСТРИРОВАТЬСЯ</button>
                       <button type="button" class="btn btn-default">ВХОД В КАБИНЕТ</button>
                   </div>
                   <div class="col-md-5">
                       <img src="image/Tana.jpg" class="tanaImage" alt="">
                           
                </div>
               </div> 
               
           </div>      
<?php require_once ('_block/_footer.php');?>
       </div>
    </body>
    
</html>
