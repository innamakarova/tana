<!DOCTYPE html>
<html>
    <head>
        <title>TA-NA</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/style.css" rel="stylesheet" type="text/css"/>

        <script src="js/jquery-1.12.4.js" type="text/javascript"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        

    </head>
    <body>
        <?php
       $activeMenu='wanted';
               ?>
        <?php
        $wantedMan=[
           'discription'=>'Описание человека',
            'sex'=>1,
            'firstName'=>'Имя',
            'birthday'=>'01.01.2010',
            'lastDay'=>'01.03.2020',
            'plays'=>'Литин',
            'whoWanted'=>'Сын',
            'address'=>'Винница',
            'contact'=>'0972174696',
            'image'=>'image/kids.jpg',
        ]
        ?>
        
       <div id="wrap"> 
       <?php require_once ('_block/_header.php');?>
           <div class="container" id="wantedAll">
               <div class="row">
                   <div class="col-md-8">
                       <div class="form-group">
                           <h4><?=$wantedMan['discription'];?></h4>             
                   <textarea  rows="8" class="form-control" id="createMessage"></textarea>
                </div> 
                   </div>
               </div>
               <div class="row">
                   <div class="col-md-8">
                      <?php
                      
                      ?>
                   </div>
               </div>
               <div class="row">
                   <div class="col-md-6">
                       <div class="row">                   
                           <div class="form-group">
                            <label class="col-md-4 control-label" for="name">
                                Дата рождения</label>
                             <div class="col-md-4">
                                       <input id="name" value="<?=$wantedMan['birthday'];?>" class="form-control" name="category" aria-invalid="false">                                                               
                             </div>
                           </div>
                       </div>
                           <div class="row">
                           <div class="form-group">
                                    <label class="col-md-4 control-label" for="name">
                                        Когда последний раз видели</label>
                                   <div class="col-md-4">
                                       <input id="name" value="<?=$wantedMan['lastDay'];?>" class="form-control" name="category" aria-invalid="false">                                                               
                                   </div>  
                           </div>
                           </div>
                       <div class="row">
                           <div class="form-group">
                                    <label class="col-md-4 control-label" for="name">
                                        Предполагамое место розыска</label>
                                   <div class="col-md-4">
                                       <input id="name" value="<?=$wantedMan['plays'];?>" class="form-control" name="category" aria-invalid="false">                                                               
                                   </div>  
                           </div>
                           </div>
                       <div class="row">
                           <div class="form-group">
                                    <label class="col-md-4 control-label" for="name">
                                        Кто разыскивает</label>
                                   <div class="col-md-4">
                                       <input id="name" value="<?=$wantedMan['whoWanted'];?>" class="form-control" name="category" aria-invalid="false">                                                               
                                   </div>  
                           </div>
                           </div>
                       <div class="row">
                           <div class="form-group">
                                    <label class="col-md-4 control-label" for="name">
                                        Предполагаемое место розыска</label>
                                   <div class="col-md-4">
                                       <input id="name" value="<?=$wantedMan['address'];?>" class="form-control" name="category" aria-invalid="false">                                                               
                                   </div>  
                           </div>
                           </div>
                       <div class="row">
                           <div class="form-group">
                                    <label class="col-md-4 control-label" for="name">
                                        Телефон ищущего</label>
                                   <div class="col-md-4">
                                       <input id="name" value="<?=$wantedMan['contact'];?>" class="form-control" name="category" aria-invalid="false">                                                               
                                   </div>
                           </div>
                           </div>
                       </div>
                   <div class="col-md-2">
                       <input type="file" name="file upload" id="file-input">
                       <a href="#"><img src="<?= $wantedMan['image']; ?>" alt=""></a>
                       <input type="button" class="btn btn-primary btn-circle" value="Подать" id="circle">
                   </div> 
               </div>
           </div>
            
        
 <?php require_once ('_block/_footer.php');?>
       </div>
           
    </body>
    
</html>

