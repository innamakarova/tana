<!DOCTYPE html>
<html>
    <head>
        <title>TA-NA</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/style.css" rel="stylesheet" type="text/css"/>

        <script src="js/jquery-1.12.4.js" type="text/javascript"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        

    </head>
    <body>
        
        
    <div id="wrap">     
       <?php require_once ('_block/_header.php');?>
        <div class="container" id="createMessage">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                                    
                   <textarea  rows="12" class="form-control" id="createMessage"></textarea>
                </div>    
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <input type="button" class="btn btn-primary" value="Опубликовать">
            </div>
        </div>
        
        
        </div>     
<?php require_once ('_block/_footer.php');?>
    </div>
    </body>
    
</html>
