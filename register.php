<!DOCTYPE html>
<html>
    <head>
        <title>TA-NA</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/style.css" rel="stylesheet" type="text/css"/>

        <script src="js/jquery-1.12.4.js" type="text/javascript"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script>


    </head>
    
    <body>
        <div id="wrap" >
            <?php require_once ('_block/_header.php'); ?>     
            <div class="container" id="register">
                <h1>
                    Зарегистрируйтесь, для того чтобы войти в личный кабинет
                </h1>
                <form>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="text"  class="form-control" name="name" placeholder="Ваше имя">
                            </div>        
                            <div class="form-group">
                                <input type="text"  class="form-control" name="email" placeholder="Ваш Email">
                            </div>        
                            <div class="form-group">
                                <input type="text"  class="form-control" name="phone" placeholder="Номер телефона">
                            </div>        
                            <div class="form-group">
                                <input type="checkbox" name="agree">
                                <label>
                                    Я принимаю <a href="/license.php" >условия использования сайта </a>
                                </label>
                            </div>    
                            <hr>
                            <div class="form-group">
                                <button type="submit" class="btn btn-success">Зарегистрироваться</button>
                            </div> 
                            <div class="form-group">
                                У меня уже есть аккаунт <a href="/login">Войти</a>
                            </div>
                            <div class="form-group">
                                <a href="/remember">Я забил пароль</a>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <img src="image/register.jpg" alt=""/>
                        </div>

                    </div>
                </form>
            </div>
            <?php require_once ('_block/_footer.php'); ?>
        </div>


    </body>

</html>
