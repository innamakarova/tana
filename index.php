<!DOCTYPE html>
<html>
    <head>
        <title>TA-NA</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/style.css" rel="stylesheet" type="text/css"/>

        <script src="js/jquery-1.12.4.js" type="text/javascript"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        

    </head>
    <body>
        
        
        
       <?php require_once ('_block/_header.php');?>
        
        <div id="carousel" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#carousel" data-slide-to="0" class="active"></li>
                <li data-target="#carousel" data-slide-to="1"></li>
                <li data-target="#carousel" data-slide-to="2"></li>
                <li data-target="#carousel" data-slide-to="3"></li>
                <li data-target="#carousel" data-slide-to="4"></li>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <div class="item active">
                    <img src="/image/slider/1.jpg" alt="...">
                    <div class="carousel-caption">
                        ...
                    </div>
                </div>
                <div class="item">
                    <img src="/image/slider/2.jpg" alt="...">
                    <div class="carousel-caption">
                        ...
                    </div>
                </div>
                <div class="item">
                    <img src="/image/slider/3.jpg" alt="...">
                    <div class="carousel-caption">
                        ...
                    </div>
                </div>
                <div class="item">
                    <img src="/image/slider/4.jpg" alt="...">
                    <div class="carousel-caption">
                        ...
                    </div>
                </div>
                <div class="item">
                    <img src="/image/slider/5.jpg" alt="...">
                    <div class="carousel-caption">
                        ...
                    </div>
                </div>
                ...
            </div>

            <!-- Controls -->
            <a class="left carousel-control" href="#carousel" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#carousel" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>

        <div id="we-can">
            <h1>Мы можем</h1>
        </div>
        <div id="main-block">
            <div class="row row-flex">
                <div class="col-md-4">
                    <div class="item-block">
                        Видеть нужду и реагировать на нее конкретной помощью
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="item-block">
                        Находить работу через биржи и надежных работодателей
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="item-block">
                        Принимать участие в волонтерской деятельности организаций и тем самым становиться полезным обществу
                    </div>
                </div>
            </div>
            <div class="row row-flex">

                <div class="col-md-4 col-md-offset-2">
                    <div class="item-block">
                        Разгружать свое жилище от вещей, которые захламляют нашу жизнь
                    </div>
                </div>
                <div class="col-md-4 ">
                    <div class="item-block">
                        Получать образование,<br>необходимое для<br>приобретения базовых<br>навыков в професии<br>
                    </div>
                </div>

            </div>
        </div>
        <div id="idea-help">
            <h1> Помощь идее</h1>
        </div>
        <div id="about">
            <p>Главная помощь участника зарегистрировавшегося на нашей платформе, понять чем каждый может быть полезен в <br>
                благотворительном движении и сделать предложение своих видений на почту : 


                <a href="mailto:ta.na.proposals@gmail.com">ta.na.proposals@gmail.com"</a> 
                Реальная<br>
                помощь начинается, когда об проекте знают массы, будем благодарны, если вы порекомендуете нас свои друзьям<br>
                и близким.<br>
            </p>
            <p>На сегодняшний день мы може рассматривать такие категории нуждающихся и благодаря данной платформе<br>
                участниками являются те, кто занимается данными категориями:<br>
            </p>
        </div>
        <div class="main-container">
            <div class="item-container">
                <div class="item-num">
                    01
                </div>
                <h1>ДЕТИ-СИРОТЫ</h1>
                <p>Даря подарки в детдома- мы даем их воспитанникам привычну иждивенцов.В<br>
                    действительности дети-сироты кроме подарков, нуждаются еще в друх вещах: в<br>
                    подготовке к самостоятельной жизни и советах взрослых, которые могут поделится<br>
                    своим опытом.<br>
                </p>
                <p>Найдите в себе желание подключится к обществу и станем вместе для ребенка<br>
                    значимыми людьми, которые покажут, как устроен мир.<br>
                </p>

            </div> 
            <div class="item-container">
                <div class="item-num">
                    02
                </div>
                <h1>БЕЗДОМНЫЕ</h1>
                <p>В связи с сезонными переменами года во время с октября по апрель, самое страшное<br>
                    время для бездомных. Многие погибают от холода и становятся инвалидами. Если вы<br>
                    увидели бездомного, жизни которому угрожает какой-то рациональный фактор нужно<br>
                    вызвать «Скорую помощь». Если же опасности для здоровья нет, обратитесь в<br>
                    социальные центры или же купите готовой еды, горячий чай и не давайте денег.<br></p>
            </div>
            <div class="item-container">
                <div class="item-num">
                    03
                </div>
                <h1>АЛКО-И НАРКО-ЗАВИСИМЫЕ</h1>
                <p>Наркомания и алкоголизм — это такие же болезни как и все остальные, разница<br>
                    только в искусственном приобретении, а людям, которые от них страдают, нужна<br>
                    помощь, оказывать которую должен профессионал.<br></p>
                <p> Помочь таким людям можно, отправив их в определенные центры реабилитации. В<br>
                    которых они должны быть чем-то заняты.<br></p>
            </div>
            <div class="item-container">
                <div class="item-num">
                    04
                </div>
                <h1>ЛЮДИ С ОГРАНИЧЕННЫМИ ФИЗИЧЕСКИМИ ВОЗМОЖНОСТЯМИ</h1>
                <p>Любой инвалид не является не полноценным, у него только физические ограничения.<br>
                    Общайтесь с людьми с физической инвалидностью так же, как вы общаетесь со<br>
                    всеми остальными людьми. Если вы хотите помочь человеку, сначала узнайте, нужна<br>
                    ли ему помощь, и если да, то как именно ему нужно помочь.<br></p>
            </div>
            <div class="item-container">
                <div class="item-num">
                    05
                </div>
                <h1>ЛЮДИ С МЕНТАЛЬНЫМИ ОСОБЕННОСТЯМИ</h1>
                <p>Люди с аутизмом или синдромом Дауна могут вести себя иначе, чем мы привыкли, и из-за этого<br>
                    они уникальны. Еще нуждаются в поддержке родственники людей с особенностями. В роддомах<br>
                    молодых мам уговаривают отказаться от таких детей. Те, кто не соглашаются, сталкиваются с<br>
                    массой предрассудков со стороны общества, например, с идеей о том, что особенные дети<br>
                    необучаемы.<br></p>
            </div>
            <div class="item-container">
                <div class="item-num">
                    06
                </div>
                <h1>ПОЖИЛЫЕ ЛЮДИ И ТЕ, КОГО ВСЕ<br> БРОСИЛИ</h1>
                <p>Люди в пожилом возрасте нуждаются в том же, в чем и дети. Чтобы помочь, можно<br>
                    присоединиться к волонтерской группе, у которой есть опыт и знание специфики<br>
                    работы с пожилыми людьми. Нам не избежать смерти и преклонных возрастов,<br>
                    каждый будет стариком и мудрецом.<br></p>
            </div>
            <div class="item-container">
                <div class="item-num">
                    07
                </div>
                <h1>ЛЮДИ С ТЯЖЕЛЫМИ ИЛИ НЕИЗЛИЧИМЫМИ БОЛЕЗНЯМИ</h1>
                <p>Заболеваний много: помимо нуждающихся в лечении от онкологии, есть люди с<br>
                    разными синдромами, которые только чудом смогут исцелится, а сейчас им нужна<br>
                    наша помощь в зависимости от возможностей каждого.<br></p>
            </div>
            <div class="item-container">
                <div class="item-num">
                    08
                </div>
                <h1>ПОСТРАДАВШИЕ В КАТАСТРОФАХ<br> ИЛИ БЕДСТВИЯХ</h1>
                <p>Первое правило — сдавать кровь, ее всегда не хватает. Если вы хотите пожертвовать<br>
                    вещи — проследите, чтобы они были чистые и целые. Если хотите стать волонтером,<br>
                    обратитесь к организациям, которые организуют волонтерскую работу. Никто не<br>
                    застрахован от превратностей судьбы, отнеситесь с пониманием и ответной<br>
                    реакцией.<br></p>
            </div>
            <div class="item-container">
                <div class="item-num">
                    09
                </div>
                <h1>ЖИВОТНЫЕ</h1>
                <p>Давайте не будем забывать и о братьях наших младших, ведь они те существа,<br>
                    которые никак не смогут попросить помощи. Про то, что подавать милостыню<br>
                    попрошайкам с собаками нельзя, вы уже знаете. Главное — не отдавать питомца в<br>
                    не проверенный приют, потому что наши приюты больше напоминают собачий<br>
                    концлагерь.Если вы захотите завести себе питомца, то лучше подобрите их на улице<br>
                    или в существующем приюте: поскольку искусственно выведенные животные<br>
                    предрасположены к генетическим заболеваниям и нарушениям поведения минимум,<br>
                    а максимум: Зачем покупать животное за космические цены?<br></p>
                <p>Его преданность, вы не купите!<br></p>
            </div>
        </div>
        <div id="partners">
            <h1>Наши партнёры</h1>
        </div>
        <div class="row">
            <div class="col-md-3">
                <a href="index.php.html" class="item-partner">
                    <img src="\image\рисунок.png" class="img-circle" alt="">
                </a>
            </div>
            <div class="col-md-3">
                <a href="index.php.html" class="item-partner">
                    <img src="\image\рисунок.png" class="img-circle" alt="">
                </a>
            </div>
            <div class="col-md-3">
                <a href="index.php.html" class="item-partner">
                    <img src="\image\рисунок.png" class="img-circle" alt="">
                </a>
            </div>
            <div class="col-md-3">
                <a href="index.php.html" class="item-partner">
                    <img src="\image\рисунок.png" class="img-circle" alt="">
                </a>
            </div>
        </div>
        <div id="news">
            <h1>Новости</h1>

            <div class="row">
                <div class="col-md-8">
                    <img src="image/news/news1.jpeg" class="item-news" alt="">
                </div>
                <div class="col-md-4">
                    <h3>НАШИ ВОЛОНТЁРЫ ПОМОГЛИ  
                        ОЧИСТИТЬ ПЛЯЖ ОТ МУСОРА </h3>
                    <p>Многие из нас, гуляя на природе, 
                        наблюдают грустную картину: в 
                        лесу и особенно на берегах рек и 
                        озер валяются упаковки от 
                        чипсов, пустые бутылки, пакеты... 
                        Зачастую этот мусор не убирают 
                        годами, и он, накапливаясь, 
                        превращается в уродливые 
                        зловонные мини-свалки — ведь 
                        во многих случаях прибрежные 
                        территории являются 
                        бесхозными. В этой ситуации есть 
                        всего один выход — засучить 
                        рукава и убрать все своими 
                        силами. </p>
                    <p class="next-link"><a href="#"><h3> ЧИТАТЬ ДАЛЬШЕ &rarr;</h3></a></p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <h3>ДЕТИ - НАШЕ БУДУЩЕЕ!</h3>
                    <p>В рамках благотворительной 
                        акции "Подари улыбку" студенты 
                        Сумского государственного 
                        университета посетили Сумский 
                        детский дом им. С.П. Супруна. На 
                        вырученные средства и при 
                        содействии добровольцев 
                        волонтеры привезли одежду, 
                        игрушки, канцелярские товары, 
                        сладости, а также – развлечения 
                        и позитивное настроение. </p>
                    
                    <p class="next-link"><a href="#"><h3> ЧИТАТЬ ДАЛЬШЕ &rarr;</h3></a></p>
                </div>
                <div class="col-md-8">
                    <img src="image/news/news2.jpeg" class="item-news" alt="">
                </div>
            </div> 
        </div>
        <?php require_once ('_block/_footer.php');?>
    </body>
    
</html>
