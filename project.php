<!DOCTYPE html>
<html>
    <head>
        <title>TA-NA</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/style.css" rel="stylesheet" type="text/css"/>

        <script src="js/jquery-1.12.4.js" type="text/javascript"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script>


    </head>
    <body>


        <?php
        $activeMenu = 'project';
        ?>
        <?php
        $project = [
                ['text' => 'Видеть нужду и реагировать на неё конкретной помощью', 'img' => 'image/handproject.png', 'id' => 'palets1'],
                ['text' => 'Получено образование, необходимое для приобритения базовых навыков в професии', 'img' => 'image/handproject.png', 'id' => 'palets2'],
                ['text' => 'Принимать участие в волонтёрской деятельности организаций и тем самым становиться полезным обществу', 'img' => 'image/handproject.png', 'id' => 'palets3'],
                ['text' => 'Найти пропавшего близкого или родного человека', 'img' => 'image/handproject.png', 'id' => 'palets4'],
                ['text' => 'Разгружать своё жилище от вещей, которые захламляют нашу жизнь', 'img' => 'image/handproject.png', 'id' => 'palets5'],
                ]
        ?>
        <div id="wrap">
            <?php require_once ('_block/_header.php'); ?>
            <div class="container" id="item-project">
                <div id="project-title">
                    <h1>С помощью ТА, каждый может стать частью благотворительности и быть альтруистом не отходя от монитора</h1> 
                </div>
                <div id="project-body">
                    <div id="project-hand">
                        <img src="image/hand.png"  alt="">

                        <h2>Мы можем</h2>
                    </div>
                    <div class="project-paltsi">
                        <?php foreach ($project as $projectItem): ?>
                        <div id="<?= $projectItem['id'] ?>" class="palets">
                                <h4><?= $projectItem['text']; ?> </h4>
                                <a href="#"><img src="<?= $projectItem['img']; ?>"  alt="" ></a>
                            </div>
                        <?php endforeach; ?>

                    </div>
                </div>

            </div>

            <?php require_once ('_block/_footer.php'); ?>

        </div>
    </body>

</html>

